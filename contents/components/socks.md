---
name: Socks
status: built
vueComponents: []
related: []
---

Socks represent a unique way to warm your feet.  

## Usage

Socks can be used universally in almost every environment.

In some bare-foot contexts like Beaches, Socks shouldn't be used.

## Categories

Usually different sock categories help warming either the left or the right foot.

### Universal

At GitLab we chose to implement a universal sock design that can be used in both use cases.

![GitLab socks](/img/component-socks.jpg)
